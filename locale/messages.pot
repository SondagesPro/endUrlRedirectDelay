msgid ""
msgstr ""
"Project-Id-Version: endUrlRedirectDelay\n"
"POT-Creation-Date: 2017-12-15 16:12+0100\n"
"PO-Revision-Date: 2017-12-15 16:12+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.3\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _translate\n"
"X-Poedit-SearchPath-0: .\n"

#: endUrlRedirectDelay.php:50
msgid "Delay for redirection"
msgstr ""

#: endUrlRedirectDelay.php:51
msgid "0 or empty deactivate the redirection"
msgstr ""

#: endUrlRedirectDelay.php:56
msgid "Url for redirection"
msgstr ""

#: endUrlRedirectDelay.php:57
msgid "By default use the End URL"
msgstr ""

#: endUrlRedirectDelay.php:62
msgid "Redirect to start new current survey"
msgstr ""

#: endUrlRedirectDelay.php:63
msgid "This setting have priority"
msgstr ""
